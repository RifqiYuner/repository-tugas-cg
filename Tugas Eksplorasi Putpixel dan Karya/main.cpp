#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define GetCurrentDir getcwd
int main()
{
    char buff[FILENAME_MAX];
    GetCurrentDir(buff, FILENAME_MAX);
    int ch;
    do
    {
        // prompt user input
        printf("******** MENU ********\n");
        printf("1. Soal Horizontal\n");
        printf("2. Garis Vertikal\n");
        printf("3. Garis Diagonal\n");
        printf("4. Smileyface\n");

        printf("\nSilahkan pilih menu?\n");
        printf("\nNB: Menu ini hanya akan berjalan pada device yang sudah mengkonfigurasi environtment system MinGW dan graphics.h\n\n");

        scanf("%d", &ch);

        switch (ch)
        {
        case 1:
        {
            char args[] = "g++ ";
            strcat(args, buff);
            strcat(args, "\\garishorizontal.cpp -o garishorizontal.exe -lbgi -lgdi32 -lcomdlg32 -luuid -loleaut32 -lole32 -lwinmm");
            system(args);
            system("garishorizontal.exe");
            
        }

        break;

        case 2:
        {
            char args[] = "g++ ";
            strcat(args, buff);
            strcat(args, "\\garisvertikal.cpp -o garisvertikal.exe -lbgi -lgdi32 -lcomdlg32 -luuid -loleaut32 -lole32 -lwinmm");
            system(args);
            system("garisvertikal.exe");
        }

        break;

        case 3:
        {
            char args[] = "g++ ";
            strcat(args, buff);
            strcat(args, "\\garisdiagonal.cpp -o garisdiagonal.exe -lbgi -lgdi32 -lcomdlg32 -luuid -loleaut32 -lole32 -lwinmm");
            system(args);
            system("garisdiagonal.exe");
        }

        break;

        case 4:
        {
            char args[] = "g++ ";
            strcat(args, buff);
            strcat(args, "\\smileyface.cpp -o smileyface.exe -lbgi -lgdi32 -lcomdlg32 -luuid -loleaut32 -lole32 -lwinmm");
            system(args);
            system("smileyface.exe");
        }

        break;

        }

    } while (ch != 4);

    getch();

    return 0;
}