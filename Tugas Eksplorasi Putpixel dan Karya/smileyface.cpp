#include <graphics.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

void hurufM()
{
    line(70, 150, 90, 50);
    line(90, 50, 110, 150);
    line(110, 150, 130, 50);
    line(130, 50, 150, 150);
}

void hurufA()
{
    line(170, 150, 190, 50);
    line(180, 100, 200, 100);
    line(190, 50, 210, 150);
}

void hurufN()
{
    line(230, 50, 230, 150);
    line(230, 50, 270, 150);
    line(270, 150, 270, 50);
}

void smileyFace()
{
    circle(175, 220, 50);
    circle(155, 200, 10);
    circle(195, 200, 10);
    arc(170, 230, 0, 180, 5);
    arc(180, 230, 0, 180, 5);
    arc(175, 230, 180, 360, 25);
}
int main(void)
{
    /* request auto detection */
    initwindow(350, 350, "Karya Smiley Face");

    /* draw a rectangle */

    rectangle(40, 50, 300, 150);
    hurufM();
    hurufA();
    hurufN();
    smileyFace();
    /* clean up */
    getch();
    closegraph();
    return 0;
}
