#include <graphics.h>
#include <stdio.h>
#include <windows.h>

int main()
{
    initwindow(400, 400, "First Sample");
    int xc_ = 100, yc_ = 100;
    int x = 0, y = 100;
    int decesionParameter = 3 - 2 * yc_;
    putpixel(xc_ + x, yc_ + y, WHITE);
    putpixel(xc_ - x, yc_ + y, WHITE);
    putpixel(xc_ + x, yc_ - y, WHITE);
    putpixel(xc_ - x, yc_ - y, WHITE);
    putpixel(xc_ + y, yc_ + x, WHITE);
    putpixel(xc_ - y, yc_ + x, WHITE);
    putpixel(xc_ + y, yc_ - x, WHITE);
    putpixel(xc_ - y, yc_ - x, WHITE);
    while (y >= x)
    {
        x++;
        if (decesionParameter > 0)
        {
            y--;
            decesionParameter = decesionParameter + 4 * (x - y) + 10;
        }
        else
            decesionParameter = decesionParameter + 4 * x + 6;
        putpixel(xc_ + x, yc_ + y, WHITE);
        putpixel(xc_ - x, yc_ + y, WHITE);
        putpixel(xc_ + x, yc_ - y, WHITE);
        putpixel(xc_ - x, yc_ - y, WHITE);
        putpixel(xc_ + y, yc_ + x, WHITE);
        putpixel(xc_ - y, yc_ + x, WHITE);
        putpixel(xc_ + y, yc_ - x, WHITE);
        putpixel(xc_ - y, yc_ - x, WHITE);
        delay(30);
    }

    while (!kbhit())
    {
        delay(200);
    }
    return 0;
}