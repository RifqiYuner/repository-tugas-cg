#include "stroke.h"
#include <stdio.h>
int abs(int n)
{
    return ((n > 0) ? n : (n * (-1)));
}

void DrawNormalLine(int X0, int Y0, int X1, int Y1, int color)
{

    color = abs(color);
    if (color > 15)
    {
        color = 7;
    }

    int dx = X1 - X0;
    int dy = Y1 - Y0;

    int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    float Xinc = dx / (float)steps;
    float Yinc = dy / (float)steps;

    float X = X0;
    float Y = Y0;
    for (int i = 0; i <= steps; i++)
    {
        putpixel(X, Y, color);
        X += Xinc;
        Y += Yinc;
    }
}

void DrawPointLine(int X0, int Y0, int X1, int Y1, int Separated, int color)
{

    color = abs(color);
    if (color > 15)
    {
        color = 7;
    }

    int dx = X1 - X0;
    int dy = Y1 - Y0;

    int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    float Xinc = dx / (float)steps;
    float Yinc = dy / (float)steps;

    float X = X0;
    float Y = Y0;
    for (int i = 0; i <= steps; i++)
    {
        putpixel(X, Y, color);
        X += Xinc * Separated;
        Y += Yinc * Separated;
    }
}

void DrawDashedLine(int X0, int Y0, int X1, int Y1, int Dash, int color)
{

    color = abs(color);
    if (color > 15)
    {
        color = 7;
    }

    int dx = X1 - X0;
    int dy = Y1 - Y0;

    int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    float Xinc = dx / (float)steps;
    float Yinc = dy / (float)steps;

    float X = X0;
    float Y = Y0;
    for (int i = 0; i <= steps; i++)
    {
        if (i % Dash == 0)
        {
            putpixel(X, Y, color);
            X += Xinc * Dash;
            Y += Yinc * Dash;
        }
        else
        {
            putpixel(X, Y, color);
            X += Xinc;
            Y += Yinc;
        }
    }
}

void DrawDashedPointLine(int X0, int Y0, int X1, int Y1, int Dash, int Separated, int color)
{

    color = abs(color);
    if (color > 15)
    {
        color = 7;
    }

    int dx = X1 - X0;
    int dy = Y1 - Y0;

    int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    float Xinc = dx / (float)steps;
    float Yinc = dy / (float)steps;
    bool Titik = false;
    float X = X0;
    float Y = Y0;
    int i = 0;
    while (i <= steps)
    {
        if (Titik == true)
        {
            for (int j = 0; j <= Separated && i <= steps; j++)
            {
                putpixel(X, Y, color);
                X += Xinc * 4;
                Y += Yinc * 4;
                i++;
            }
            Titik = false;
        }
        if (Titik == false)
        {

            for (int j = 0; j <= Dash && i <= steps; j++)
            {
                putpixel(X, Y, color);
                X += Xinc;
                Y += Yinc;
                i++;
            }
            Titik = true;
        }
    }
}

void garisTebal(int X0, int Y0, int X1, int Y1, int thicc, int color)
{
    color = abs(color);
    if (color > 15)
    {
        color = 7;
    }

    int dx = X1 - X0;
    int dy = Y1 - Y0;

    int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    float Xinc = dx / (float)steps;
    float Yinc = dy / (float)steps;

    float x = X0;
    float y = Y0;
    for (int i = 0; i <= steps; i++)
    {

        int x2 = x, y2 = y;
        for (int j = 1; j <= thicc; j++)
        {
            putpixel(x2, y2, color);
            if (dx / dy < 1 && dy > 0)
            {
                x2++;
                y2 = (dx / dy) * x2 + (y2 - (dx / dy) * x2);
            }
            else if (dx / dy > 1 && dy > 0)
            {
                 y2++;
                 x2 = (y2 - (y2 - (dx / dy) * x2))/(dx/dy);
            }
            else
            {
                y2++;
            }
           
        }

        x += Xinc;
        y += Yinc;
    }
}