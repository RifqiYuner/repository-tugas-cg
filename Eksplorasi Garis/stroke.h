#include <graphics.h>

int abs(int n);
void DrawNormalLine(int x0,int y0, int x1, int y1, int color=9);
void DrawPointLine(int X0, int Y0, int X1, int Y1, int Separated, int color=9);
void DrawDashedLine(int X0, int Y0, int X1, int Y1, int Dash, int color=9);
void DrawDashedPointLine(int X0, int Y0, int X1, int Y1, int Dash,int Separated, int color=9);
void garisTebal(int X0, int Y0, int X1, int Y1, int thicc, int color=9);
/*
Semua manipulasi garis menggunakan algoritma DDA dengan pengecualian garisTebal menggunakan gabungan DDA dan Bresenham untuk penebalannya

*/

/*
Terdapat kesalahan rumus pada garis tebal, seharusnya menggunakan dy/dx, tetapi saya menggunakan dx/dy dan entah kenapa 
hasil dari dy/dx tidak sesuai dengan harapan sedangkan untuk dx/dy sesuai dengan harapan

Penulis belum sempat menganalisis masalah tersebut dan belum sempat memperbaikinya dikarenakan deadline yang sudah dekat
*/

/*
Garis tebal memerlukan waktu render yang cukup lama dikarenakan loop yang telalu banyak apabila garis terlalu tebal
*/