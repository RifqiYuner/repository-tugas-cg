#include "shape.h"

/*
Manipulasi garis untuk membentuk persegi dengan meminta input koordinat awal garis akan dibuat dan memasukkan panjang sisinya
*/
void DrawPersegi(int X0, int Y0, int Sisi, int Color)
{
    DrawNormalLine(X0, Y0, X0 + Sisi, Y0, Color);
    DrawNormalLine(X0, Y0, X0, Y0 + Sisi, Color);
    DrawNormalLine(X0 + Sisi, Y0, X0 + Sisi, Y0 + Sisi, Color);
    DrawNormalLine(X0, Y0 + Sisi, X0 + Sisi, Y0 + Sisi, Color);
}
/*
Manipulasi garis untuk membentuk persegi panjang dengan meminta input koordinat awal garis akan dibuat dan memasukkan panjang dan lebar sisinya
*/
void DrawPersegiPanjang(int X0, int Y0, int Lebar, int Panjang, int Color)
{
    DrawNormalLine(X0, Y0, X0 + Panjang, Y0, Color);
    DrawNormalLine(X0, Y0, X0, Y0 + Lebar, Color);
    DrawNormalLine(X0 + Panjang, Y0, X0 + Panjang, Y0 + Lebar, Color);
    DrawNormalLine(X0, Y0 + Lebar, X0 + Panjang, Y0 + Lebar, Color);
}

/*
Manipulasi garis untuk membentuk Jajaran Genjang dengan meminta input koordinat kiri atas jajaran genjang dan koordinat kiri bawah jajaran genjang
dan meminta panjang sisi dari jajaran genjang tersebut (panjang sisi sejajar atas dan bawah)
*/
void DrawJajarGenjang(int X0, int Y0, int X1, int Y1, int PanjangSisi, int Color)
{
    if (X0 == X1)
    {
        printf("Koordinat X sama sehingga tidak akan membentuk Jajaran Genjang!");
    }
    DrawNormalLine(X0, Y0, X0 + PanjangSisi, Y0, Color);
    DrawNormalLine(X1, Y1, X1 + PanjangSisi, Y1, Color);
    DrawNormalLine(X0, Y0, X1, Y1, Color);
    DrawNormalLine(X0 + PanjangSisi, Y0, X1 + PanjangSisi, Y1, Color);
}

/*
Manipulasi garis untuk membentuk Trapesium dengan meminta input koordinat kiri atas Trapesium dan koordinat kiri bawah Trapesium
dan meminta panjang sisi atas dan panjang sisi bawah Trapesium
*/
void DrawTrapesium(int X0, int Y0, int X1, int Y1, int PanjangSisiAtas, int PanjangSisiBawah, int Color)
{
    DrawNormalLine(X0, Y0, X0 + PanjangSisiAtas, Y0, Color);
    DrawNormalLine(X1, Y1, X1 + PanjangSisiBawah, Y1, Color);
    DrawNormalLine(X0, Y0, X1, Y1, Color);
    DrawNormalLine(X0 + PanjangSisiAtas, Y0, X1 + PanjangSisiBawah, Y1, Color);
}

/*
Manipulasi garis untuk membentuk Layang - layang dengan meminta input koordinat titik tertinggi Layang - laayng dan koordinat paling kiri Layang -layang
dan meminta lebar serta tinggi dari Layang - layang
*/
void DrawLayang(int X0, int Y0, int X1, int Y1, int Tinggi, int Color)
{
    DrawNormalLine(X0, Y0, X1, Y1, Color);
    DrawNormalLine(X0, Y0, X1 + ((X0-X1)*2), Y1, Color);
    DrawNormalLine(X1, Y1, X0, Y1 + Tinggi, Color);
    DrawNormalLine(X1 + ((X0-X1)*2), Y1, X0, Y1 + Tinggi, Color);
}

/*
Manipulasi garis untuk membentuk Belah ketupat dengan meminta input koordinat awal Belahketupat
dan panjang setengah diagonal dari belah ketupat
*/
void DrawBelahKetupat(int X0, int Y0, int DiagonalSetengah, int Color)
{
    DrawNormalLine(X0, Y0, X0-DiagonalSetengah, Y0+DiagonalSetengah, Color);
    DrawNormalLine(X0, Y0, X0+DiagonalSetengah,Y0+DiagonalSetengah, Color);
    DrawNormalLine(X0+DiagonalSetengah,Y0+DiagonalSetengah, X0, Y0+(DiagonalSetengah*2), Color);
    DrawNormalLine( X0-DiagonalSetengah, Y0+DiagonalSetengah, X0, Y0+(DiagonalSetengah*2), Color);
}

/*
Manipulasi garis untuk membentuk Segitiga siku - siku dengan meminta input koordinat awal Segitiga siku - siku
dan alas segitiga serta tingginya
*/
void DrawSegitigaSiku(int X0, int Y0, int Alas, int Tinggi, int Color)
{
    DrawNormalLine(X0, Y0, X0, Y0+Tinggi, Color);
    DrawNormalLine(X0, Y0, X0+Alas, Y0+Tinggi, Color);
    DrawNormalLine(X0, Y0+Tinggi, X0+Alas, Y0+Tinggi, Color);
}