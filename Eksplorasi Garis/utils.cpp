#include "utils.h"

int Menu()
{

    int ch;
    do
    {
        // prompt user input
        printf("******** MENU ********\n");
        printf("1. Demo Stroke.h\n");
        printf("2. Demo Shape.h\n");
        printf("3. Exit\n");

        printf("\nSilahkan pilih menu?\n");

        scanf("%d", &ch);

        switch (ch)
        {
        case 1:
        {
            initwindow(400, 400, "Manipulasi Garis");
            DrawNormalLine(1, 1, 399, 399, 7);
            DrawPointLine(399, 1, 1, 399, 9);
            DrawDashedLine(1, 200, 399, 200, 9, 11);
            DrawDashedPointLine(1, 230, 399, 230, 50, 10, 5);
            garisTebal(180, 0, 180, 399, 40, 2);

            getch();
            closegraph();
        }

        break;

        case 2:
        {
            initwindow(400, 400, "Manipulasi Shape");
            DrawPersegi(200, 200, 100);
            delay(1000);
            cleardevice();
            DrawPersegiPanjang(40, 200, 50, 100);
             delay(1000);
            cleardevice();
            DrawJajarGenjang(150, 150, 125, 250, 100);
             delay(1000);
            cleardevice();
            DrawTrapesium(100, 100, 50, 150, 50, 150);
             delay(1000);
            cleardevice();
            DrawLayang(200, 200, 170, 250, 150);
             delay(1000);
            cleardevice();
            DrawBelahKetupat(200, 200, 50);
            delay(1000);
            cleardevice();
            DrawSegitigaSiku(200, 200, 100, 100);
            getch();
            closegraph();
        }

        break;

        case 3:
        {
            return 0;
        }

        break;
        }

    } while (ch != 3);

    return 0;
}