#include "shape.h"

float A[10][2];
void DrawPersegi(int X0, int Y0, int Sisi, int Color, float Scalex, float Scaley, int TranslasiX, int TranslasiY, int Sudut)
{
    A[0][0] = X0;
    A[0][1] = Y0;
    A[1][0] = X0 + Sisi;
    A[1][1] = Y0;
    A[2][0] = X0 + Sisi;
    A[2][1] = Y0 + Sisi;
    A[3][0] = X0;
    A[3][1] = Y0 + Sisi;
    Scale(Scalex, Scaley, X0, Y0, 4);
    Translasi(TranslasiX, TranslasiY, 4);
    Rotasi(Sudut, 3, X0+(Sisi/2), Y0+(Sisi/2));
    DrawNormalLine(A[0][0], A[0][1], A[1][0], A[1][1], Color);
    DrawNormalLine(A[1][0], A[1][1], A[2][0], A[2][1], Color);
    DrawNormalLine(A[2][0], A[2][1], A[3][0], A[3][1], Color);
    DrawNormalLine(A[3][0], A[3][1], A[0][0], A[0][1], Color);
}

void DrawPersegiPanjang(int X0, int Y0, int Lebar, int Panjang, int Color, float Scalex, float Scaley, int TranslasiX, int TranslasiY)
{
    A[0][0] = X0;
    A[0][1] = Y0;
    A[1][0] = X0 + Panjang;
    A[1][1] = Y0;
    A[2][0] = X0 + Panjang;
    A[2][1] = Y0 + Lebar;
    A[3][0] = X0;
    A[3][1] = Y0 + Lebar;
    Scale(Scalex, Scaley, X0, Y0, 4);
    Translasi(TranslasiX, TranslasiY, 4);
    DrawNormalLine(A[0][0], A[0][1], A[1][0], A[1][1], Color);
    DrawNormalLine(A[1][0], A[1][1], A[2][0], A[2][1], Color);
    DrawNormalLine(A[2][0], A[2][1], A[3][0], A[3][1], Color);
    DrawNormalLine(A[3][0], A[3][1], A[0][0], A[0][1], Color);
}

/*
Manipulasi garis untuk membentuk Jajaran Genjang dengan meminta input koordinat kiri atas jajaran genjang dan koordinat kiri bawah jajaran genjang
dan meminta panjang sisi dari jajaran genjang tersebut (panjang sisi sejajar atas dan bawah)
*/
void DrawJajarGenjang(int X0, int Y0, int X1, int Y1, int PanjangSisi, int Color, float Scalex, float Scaley, int TranslasiX, int TranslasiY)
{
    A[0][0] = X0;
    A[0][1] = Y0;
    A[1][0] = X0 + PanjangSisi;
    A[1][1] = Y0;
    A[2][0] = X1;
    A[2][1] = Y1;
    A[3][0] = X1 + PanjangSisi;
    A[3][1] = Y1;
    Scale(Scalex, Scaley, X0, Y0, 4);
    Translasi(TranslasiX, TranslasiY, 4);
    DrawNormalLine(A[0][0], A[0][1], A[1][0], A[1][1], Color);
    DrawNormalLine(A[2][0], A[2][1], A[3][0], A[3][1], Color);
    DrawNormalLine(A[0][0], A[0][1], A[2][0], A[2][1], Color);
    DrawNormalLine(A[1][0], A[1][1], A[3][0], A[3][1], Color);
}

/*Manipulasi garis untuk membentuk Trapesium dengan meminta input koordinat kiri atas Trapesium dan koordinat kiri bawah Trapesium
dan meminta panjang sisi atas dan panjang sisi bawah Trapesium
*/
void DrawTrapesium(int X0, int Y0, int X1, int Y1, int PanjangSisiAtas, int PanjangSisiBawah, int Color, float Scalex, float Scaley, int TranslasiX, int TranslasiY)
{
    A[0][0] = X0;
    A[0][1] = Y0;
    A[1][0] = X0 + PanjangSisiAtas;
    A[1][1] = Y0;
    A[2][0] = X1;
    A[2][1] = Y1;
    A[3][0] = X1 + PanjangSisiBawah;
    A[3][1] = Y1;
    Scale(Scalex, Scaley, X0, Y0, 4);
    Translasi(TranslasiX, TranslasiY, 4);
    DrawNormalLine(A[0][0], A[0][1], A[1][0], A[1][1], Color);
    DrawNormalLine(A[2][0], A[2][1], A[3][0], A[3][1], Color);
    DrawNormalLine(A[0][0], A[0][1], A[2][0], A[2][1], Color);
    DrawNormalLine(A[1][0], A[1][1], A[3][0], A[3][1], Color);
}

void DrawLayang(int X0, int Y0, int X1, int Y1, int Tinggi, int Color, float Scalex, float Scaley, int TranslasiX, int TranslasiY)
{
    A[0][0] = X0;
    A[0][1] = Y0;
    A[1][0] = X0;
    A[1][1] = Y1 + Tinggi;
    A[2][0] = X1;
    A[2][1] = Y1;
    A[3][0] = X1 + ((X0 - X1) * 2);
    A[3][1] = Y1;
    Scale(Scalex, Scaley, X0, Y0, 4);
    Translasi(TranslasiX, TranslasiY, 4);
    DrawNormalLine(A[2][0], A[2][1], A[1][0], A[1][1], Color);
    DrawNormalLine(A[0][0], A[0][1], A[3][0], A[3][1], Color);
    DrawNormalLine(A[0][0], A[0][1], A[2][0], A[2][1], Color);
    DrawNormalLine(A[1][0], A[1][1], A[3][0], A[3][1], Color);
}

void DrawLingkaran(int xc_, int yc_, int radius, int thicc, float ScaleX, float Scaley, int TranslasiX, int TranslasiY, int color)
{
    int i = 0;
    int originRadius = radius;

    while (i <= thicc)
    {
        /* code */ int x = 0;
        int decesionParameter = 3 - 2 * yc_;
        A[0][0] = xc_ + x;
        A[0][1] = yc_ + radius;
        A[1][0] = xc_ - x;
        A[1][1] = yc_ + radius;
        A[2][0] = xc_ + x;
        A[2][1] = yc_ - radius;
        A[3][0] = xc_ - x;
        A[3][1] = yc_ - radius;
        A[4][0] = xc_ + radius;
        A[4][1] = yc_ + x;
        A[5][0] = xc_ - radius;
        A[5][1] = yc_ + x;
        A[6][0] = xc_ + radius;
        A[6][1] = yc_ - x;
        A[7][0] = xc_ - radius;
        A[7][1] = yc_ - x;
        Scale(ScaleX, Scaley, xc_, yc_, 8);
        Translasi(TranslasiX, TranslasiY, 8);
        putpixel(A[0][0], A[0][1], color);
        putpixel(A[1][0], A[1][1], color);
        putpixel(A[2][0], A[2][1], color);
        putpixel(A[3][0], A[3][1], color);
        putpixel(A[4][0], A[4][1], color);
        putpixel(A[5][0], A[5][1], color);
        putpixel(A[6][0], A[6][1], color);
        putpixel(A[7][0], A[7][1], color);
        while (radius >= x)
        {
            x = x + 1;

            if (decesionParameter > 0)
            {
                radius--;
                decesionParameter = decesionParameter + 4 * (x - radius) + 10;
            }
            else
            {

                decesionParameter = decesionParameter + 4 * x + 6;
            }
            A[0][0] = xc_ + x;
            A[0][1] = yc_ + radius;
            A[1][0] = xc_ - x;
            A[1][1] = yc_ + radius;
            A[2][0] = xc_ + x;
            A[2][1] = yc_ - radius;
            A[3][0] = xc_ - x;
            A[3][1] = yc_ - radius;
            A[4][0] = xc_ + radius;
            A[4][1] = yc_ + x;
            A[5][0] = xc_ - radius;
            A[5][1] = yc_ + x;
            A[6][0] = xc_ + radius;
            A[6][1] = yc_ - x;
            A[7][0] = xc_ - radius;
            A[7][1] = yc_ - x;
            Scale(ScaleX, Scaley, xc_, yc_, 8);
            Translasi(TranslasiX, TranslasiY, 8);
            putpixel(A[0][0], A[0][1], color);
            putpixel(A[1][0], A[1][1], color);
            putpixel(A[2][0], A[2][1], color);
            putpixel(A[3][0], A[3][1], color);
            putpixel(A[4][0], A[4][1], color);
            putpixel(A[5][0], A[5][1], color);
            putpixel(A[6][0], A[6][1], color);
            putpixel(A[7][0], A[7][1], color);
        }
        i++;
        radius = originRadius - 1;
        originRadius = radius;
    }
}

void DrawElipse(int xc, int yc, int rx, int ry, float ScaleX, float Scaley, int TranslasiX, int TranslasiY, int Sudut)
{
    float dx, dy, d1, d2, x, y;
    x = 0;
    y = ry;
    d1 = (ry * ry) - (rx * rx * ry) + (0.25 * rx * rx);
    dx = 2 * ry * ry * x;
    dy = 2 * rx * rx * y;

    while (dx < dy)
    {

        A[0][0] = x + xc;
        A[0][1] = y + yc;
        A[1][0] = -x + xc;
        A[1][1] = y + yc;
        A[2][0] = x + xc;
        A[2][1] = -y + yc;
        A[3][0] = -x + xc;
        A[3][1] = -y + yc;
        Scale(ScaleX, Scaley, xc, yc, 8);
        Translasi(TranslasiX, TranslasiY, 8);
        Rotasi(Sudut, 8, xc, yc);
        putpixel(A[0][0], A[0][1], 15);
        putpixel(A[1][0], A[1][1], 14);
        putpixel(A[2][0], A[2][1], 13);
        putpixel(A[3][0], A[3][1], 12);
        if (d1 < 0)
        {
            x++;
            dx = dx + (2 * ry * ry);
            d1 = d1 + dx + (ry * ry);
        }
        else
        {
            x++;
            y--;
            dx = dx + (2 * ry * ry);
            dy = dy - (2 * rx * rx);
            d1 = d1 + dx - dy + (ry * ry);
        }
    }

    d2 = ((ry * ry) * ((x + 0.5) * (x + 0.5))) + ((rx * rx) * ((y - 1) * (y - 1))) - (rx * rx * ry * ry);

    while (y >= 0)
    {
        A[4][0] = x + xc;
        A[4][1] = y + yc;
        A[5][0] = -x + xc;
        A[5][1] = y + yc;
        A[6][0] = x + xc;
        A[6][1] = -y + yc;
        A[7][0] = -x + xc;
        A[7][1] = -y + yc;
        Scale(ScaleX, Scaley, xc, yc, 8);
        Translasi(TranslasiX, TranslasiY, 8);
        Rotasi(Sudut, 8, xc, yc);
        putpixel(A[4][0], A[4][1], 11);
        putpixel(A[5][0], A[5][1], 10);
        putpixel(A[6][0], A[6][1], 9);
        putpixel(A[7][0], A[7][1], 8);
        if (d2 > 0)
        {
            y--;
            dy = dy - (2 * rx * rx);
            d2 = d2 + (rx * rx) - dy;
        }
        else
        {
            y--;
            x++;
            dx = dx + (2 * ry * ry);
            dy = dy - (2 * rx * rx);
            d2 = d2 + dx - dy + (rx * rx);
        }
    }
}

void DrawSegitigaSamaSisi(int x, int y, int sisi, float ScaleX, float Scaley, int TranslasiX, int TranslasiY, int Sudut)
{

    double t = sisi / 2 * sqrt(3);

    A[0][0] = x;
    A[0][1] = y;

    A[1][1] = y - ((int)ceil(t * 2 / 3));
    A[1][0] = x;

    A[2][0] = x + (sisi / 2);
    A[2][1] = y + ((int)ceil(t / 3));

    A[3][0] = x - (sisi / 2);
    A[3][1] = A[2][1];
    Scale(ScaleX, Scaley, x, y, 3);
    Translasi(TranslasiX, TranslasiY, 3);
    Rotasi(Sudut, 3, x, y);
    DrawNormalLine(A[1][0], A[1][1], A[2][0], A[2][1]);
    DrawNormalLine(A[2][0], A[2][1], A[3][0], A[3][1]);
    DrawNormalLine(A[3][0], A[3][1], A[1][0], A[1][1]);
}

void Scale(float Scalex, float Scaley, int FixedX, int FixedY, int Vertex)
{
    int i;
    for (i = 0; i <= Vertex; i++)
    {
        A[i][0] = A[i][0] * Scalex + FixedX * (1 - Scalex);
        A[i][1] = A[i][1] * Scaley + FixedY * (1 - Scaley);
    }
}

void Translasi(int x, int y, int type)
{
    int i;
    for (i = 0; i <= type; i++)
    {
        A[i][0] = A[i][0] + x;
        A[i][1] = A[i][1] + y;
    }
}

void Rotasi(int Sudut, int Vertex, int Xc, int Yc)
{
    int xShifted;
    int yShifted;
    int k = (Sudut * 3.14) / 180;
    for (int i = 0; i <= Vertex; i++)
    {
        xShifted = A[i][0] - Xc;
        yShifted = A[i][1] - Yc;
        double xTemp = Xc + (xShifted * cos(Sudut) - yShifted * sin(Sudut));
        double yTemp = Yc + (xShifted * sin(Sudut) + yShifted * cos(Sudut));

        if (fmod(xTemp, 1) >= 0.5)
        {
            A[i][0] = ceil(xTemp);
        }
        else
        {
            A[i][0] = floor(xTemp);
        }

        if (fmod(yTemp, 1) >= 0.5)
        {
            A[i][1] = ceil(yTemp);
        }
        else
        {
            A[i][1] = floor(yTemp);
        }
    }
}