Nama Program: Demo Ets
Penulis Program: Rifqi Yuner - 181511030 (3A-D3 Teknik Informatika 2020)
Deskripsi: Demo Ets merupakan program yang menampilkan keseluruhan progress dari awal pembelajaran CG Praktikum hingga ETS
Cara Penggunaan: 
	1. Double Click Demo_Ets.exe
	2. Pilih menu dengan angka sesuai dengan hasil karya yang ingin dilihat
	3. Jika ingin melihat karya khusus untuk ETS silahkan pilih menu 11 dengan cara mengetik angka 11 pada console yang tersedia
	4. Karya ETS akan melakukan looping hingga program diclose, sedangkan untuk karya yang lainnya dapat diclose dengan menekan enter pada saat akhir render

Repository: https://gitlab.com/RifqiYuner/repository-tugas-cg/-/tree/master/ETS%20CG

Lisensi: 

MIT License

Copyright (c) 2020 Khalid Hasan

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

