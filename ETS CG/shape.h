#include "stroke.h"
#include <stdio.h>


void DrawPersegi(int X0, int Y0, int Sisi, int color = 9,float ScaleX=1, float Scaley=1, int TranslasiX=0, int TranslasiY=0, int Sudut = 0);
void DrawPersegiPanjang(int X0, int Y0, int Lebar, int Panjang, int color = 9,float ScaleX=1, float Scaley=1, int TranslasiX=0, int TranslasiY=0);
void DrawJajarGenjang(int X0, int Y0, int X1, int Y1, int PanjangSisi, int color = 9,float ScaleX=1, float Scaley=1, int TranslasiX=0, int TranslasiY=0);
void DrawTrapesium(int X0, int Y0, int X1, int Y1, int PanjangSisiAtas, int PanjangSisiBawah, int color = 9,float ScaleX=1, float Scaley=1, int TranslasiX=0, int TranslasiY=0);
void DrawLayang(int X0, int Y0, int X1, int Y1, int Tinggi, int color = 9,float ScaleX=1, float Scaley=1, int TranslasiX=0, int TranslasiY=0);
// void DrawBelahKetupat(int X0, int Y0, int Sisi, int color = 9);
// void DrawSegitigaSiku(int X0, int Y0, int Alas, int DiagonalSetengah, int color = 9);
void DrawLingkaran(int xc_, int yc_, int radius, int thicc = 1, float ScaleX=1, float Scaley=1, int TranslasiX=0, int TranslasiY=0, int color = 15);
void DrawElipse(int xc, int yc, int rx, int ry, float ScaleX=1, float Scaley=1, int TranslasiX=0, int TranslasiY=0, int Sudut=0);
void Translasi(int x, int y, int type);
void Scale(float ScaleX, float Scaley,int FixedX,int FixedY, int Vertex);
void Rotasi(int Sudut, int Vertex, int Xc, int Yc);
// void DrawSegitigaSamaSisiNormal(int x, int y, int sisi);
// void DrawElipseRotate(int xc, int yc, int rx, int ry, int Rotate);
void DrawSegitigaSamaSisi(int x, int y, int sisi, float ScaleX=1, float Scaley=1, int TranslasiX=0, int TranslasiY=0, int Sudut=0);
// void DrawBelahKetupatSegitigaSamaSisi(int x, int y, int sisi);

// void Kembang();
// void Kembang8();