#include "utils.h"

int Menu()
{

    int ch;
    do
    {
        // prompt user input
        printf("******** MENU ********\n");
        printf("1. Demo Stroke.h\n");
        printf("2. Demo Manipulasi Persegi\n");
        printf("3. Demo Manipulasi Persegi Panjang\n");
        printf("4. Demo Manipulasi Jajaran Genjang\n");
        printf("5. Demo Manipulasi Trapesium\n");
        printf("6. Demo Manipulasi Layang\n");
        printf("7. Demo Scale\n");
        printf("8. Demo Manipulasi Lingkaran\n");
        printf("9. Demo Manipulasi Elipse\n");
        printf("10. Demo Manipulasi Segitiga\n");
        printf("11. Demo Manipulasi Japanese Crest Sanada Family\n");
        printf("12. Exit\n");

        printf("\nSilahkan pilih menu?\n");

        scanf("%d", &ch);

        switch (ch)
        {
        case 1:
        {
            initwindow(400, 400, "Manipulasi Garis");
            DrawNormalLine(1, 1, 399, 399, 7);
            DrawPointLine(399, 1, 1, 399, 9);
            DrawDashedLine(1, 200, 399, 200, 9, 11);
            DrawDashedPointLine(1, 230, 399, 230, 50, 10, 5);
            garisTebal(180, 0, 180, 399, 40, 2);
            getch();
            closegraph();
        }

        break;
        case 2:
        {
            initwindow(400, 400, "Manipulasi Persegi");
            DrawPersegi(200, 200, 100, 11);
            getch();
            closegraph();
        }

        break;
        case 3:
        {
            initwindow(400, 400, "Manipulasi Persegi Panjang");
            DrawPersegiPanjang(200, 200, 100, 50, 11);
            getch();
            closegraph();
        }

        break;
        case 4:
        {
            initwindow(400, 400, "Manipulasi Jajaran Genjang");
            DrawJajarGenjang(100, 100, 200, 200, 100, 11);
            getch();
            closegraph();
        }

        break;

        case 5:
        {
            initwindow(400, 400, "Manipulasi Trapesium");
            DrawTrapesium(100, 100, 50, 150, 50, 150);
            getch();
            closegraph();
        }

        break;

        case 6:
        {
            initwindow(400, 400, "Manipulasi Layang - layang");
            DrawLayang(200, 200, 170, 250, 150);
            getch();
            closegraph();
        }

        break;
        case 7:
        {
            initwindow(400, 400, "Manipulasi Scale");
            DrawLayang(50, 50, 10, 100, 100, 9, 0.5, 0.5);
            getch();
            closegraph();
        }

        break;
        case 8:
        {
            initwindow(400, 400, "Manipulasi Lingkaran");
            DrawLingkaran(200, 200, 100, 1, 2, 2);
            getch();
            closegraph();
        }

        break;

        case 9:
        {
            initwindow(400, 400, "Manipulasi Elipse");
            DrawElipse(200, 200, 50, 100, 1, 1);
            DrawElipse(200, 200, 50, 100, 2, 2);
            DrawElipse(200, 200, 50, 100, 1.3, 1.3, 20, 20);
            DrawElipse(200, 200, 50, 100, 1, 1, 0, 0, 10);
            DrawElipse(200, 200, 50, 100, 1, 1, 0, 0, 20);
            getch();
            closegraph();
        }

        break;

        case 10:
        {
            initwindow(400, 400, "Manipulasi Segitiga");
            DrawSegitigaSamaSisi(200, 200, 100, 1, 1, 0, 0, 0);
            DrawSegitigaSamaSisi(200, 200, 100, 1, 1, 0, 0, 10);
            DrawSegitigaSamaSisi(200, 200, 100, 1, 1, 0, 0, 20);
            getch();
            closegraph();
        }

        break;

        case 11:
        {
            initwindow(1000, 900, "Animasi");
            while (1)
            {
                float scale = 1;
                while (scale < 1.3)
                {
                    cleardevice();
                    int Translasi = 0, loop = 1;
                    while (loop <= 6)
                    {
                        if (loop % 2 == 1)
                        {
                            DrawLingkaran(200, 200, 100, 100 * scale, scale, scale, Translasi * scale, 0);
                            int SisiPersegi = 166;
                            int n = 60;
                            while (n >= 0)
                            {
                                DrawPersegi(SisiPersegi, SisiPersegi, n, 0, scale, scale, Translasi * scale, 0);
                                n--;
                                SisiPersegi++;
                            }
                        }
                        else
                        {
                            DrawLingkaran(200, 200, 100, 100 * scale, scale, scale, Translasi * scale, 200 * scale);
                            int SisiPersegi = 166;
                            int n = 60;
                            while (n >= 0)
                            {
                                DrawPersegi(SisiPersegi, SisiPersegi, n, 0, scale, scale, Translasi * scale, 200 * scale);
                                n--;
                                SisiPersegi++;
                            }
                            Translasi = Translasi + 200;
                        }

                        loop++;
                    }
                    Translasi = 0;
                    while (loop >= 1)
                    {
                        if (loop % 2 == 1)
                        {
                            DrawLingkaran(200, 200, 100, 100 * scale, scale, scale, Translasi * scale, 0, BLACK);
                        }
                        else
                        {
                            DrawLingkaran(200, 200, 100, 100 * scale, scale, scale, Translasi * scale, 200 * scale, BLACK);

                            Translasi = Translasi + 200;
                        }

                        loop--;
                    }
                    scale = scale + 0.1;
                }
            }

            getch();
            closegraph();
        }

        break;
        case 12:
        {
            return 0;
        }

        break;
        }

    } while (ch != 12);

    return 0;
}