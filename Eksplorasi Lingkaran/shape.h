#include "stroke.h"
#include <stdio.h>
void DrawPersegi(int X0, int Y0, int Sisi, int color=9);
void DrawPersegiPanjang(int X0, int Y0, int Lebar,int Panjang, int color=9);
void DrawJajarGenjang(int X0, int Y0,int X1, int Y1, int PanjangSisi, int color=9);
void DrawTrapesium(int X0, int Y0,int X1, int Y1, int PanjangSisiAtas, int PanjangSisiBawah, int color=9);
void DrawLayang(int X0, int Y0,int X1, int Y1, int Tinggi, int color=9);
void DrawBelahKetupat(int X0, int Y0, int Sisi, int color=9);
void DrawSegitigaSiku(int X0, int Y0, int Alas, int DiagonalSetengah, int color=9);