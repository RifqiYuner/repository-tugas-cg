#include <stdio.h>
#include <graphics.h>
#include <math.h>
int abs(int n)
{
    return ((n > 0) ? n : (n * (-1)));
}

void DDA(int X0, int Y0, int X1, int Y1)
{

    int dx = X1 - X0;
    int dy = Y1 - Y0;

    int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    float Xinc = dx / (float)steps;
    float Yinc = dy / (float)steps;

    float X = X0;
    float Y = Y0;
    for (int i = 0; i <= steps; i++)
    {
        putpixel(X, Y, 7);
        X += Xinc;
        Y += Yinc;
    }
}

void initBingkai()
{
    DDA(1, 1, 399, 1);
    DDA(1, 1, 1, 399);
    DDA(1, 399, 399, 399);
    DDA(399, 1, 399, 399);
}
void lineCartesius()
{
    DDA(200, 1, 200, 399);
    DDA(1, 200, 399, 200);
    settextstyle(3, 0, 1);
    outtextxy(1, 1, "1,1");
    outtextxy(359, 1, "399,1");
    outtextxy(339, 379, "399,399");
    outtextxy(1, 379, "1,399");
    printf("Titik sudut kiri atas adalah (1, 1)\nTitik sudut kanan atas adalah (399, 1)\nTitik sudut kiri bawah adalah (1, 399)\nTitik sudut kanan bawah adalah (399, 399)");
}

void xSqure()
{
    float x1 = 0;
    float x2 = 5;
    float nolX = 400 / 2;
    float nolY = 400 / 2;
    float y = 0.0;

    for (float y = nolY; y > 1; y = y - 0.1)
    {
        putpixel(nolX + x1, nolY - x1 * x1, CYAN);
        x1 = x1 + 0.1;
        if (x1 >= x2)
        {
            break;
        }
    }
}
void gelombangSin()
{
    int angle = 0;
    double x, y;
    for (x = 200; x < 399; x++)
    {
        y = 50 * sin(angle * 3.141 / 180);

        y = getmaxy() / 2 - y;

        putpixel(x, y, 15);

        angle += 5;
    }
}

void gelombangCos()
{
    int angle = 0;
    double x, y;
    for (x = 200; x < 399; x++)
    {
        y = 50 * cos(angle * 3.141 / 180);

        y = getmaxy() / 2 - y;

        putpixel(x, y, 9);

        angle += 5;
    }
}

void yXPangkat3()
{

    for (float x = 0; x <= 399; x += 0.01)
    {
        float x_last = (400 / 2) + x;
        float y_last = (400 / 2) - (x * x * x) - 3 * x - 1;
        putpixel(x_last, y_last, RED);
    }
}
int main()
{

    initwindow(400, 400, "WinBGI");
    initBingkai();
    lineCartesius();
    xSqure();
    gelombangSin();
    gelombangCos();
    yXPangkat3();
    getch();
    return 0;
}